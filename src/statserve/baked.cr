require "baked_file_system"

module Statserve
  module Baked
    class FileStore
      extend BakedFileSystem

      {% if env("BAKE_DIR").starts_with?('/') %}
        bake_folder {{env("BAKE_DIR")}}
      {% else %}
        bake_folder {{"../../#{env("BAKE_DIR").id}"}}
      {% end %}
    end

    class FileHandler
      include HTTP::Handler

      def call(context)
        path = context.request.path
        path = "#{path}index.html" if path.ends_with?('/')
        file = FileStore.get?(path)
        return call_next(context) if file.nil?

        context.response.headers["Content-Type"] = file.mime_type

        if context.request.headers.includes_word?("Accept-Encoding", "gzip")
          context.response.headers["Content-Encoding"] = "gzip"
          context.response.write(file.to_slice)
        else
          IO.copy(file, context.response)
        end
      end
    end

    def file_handler
      FileHandler.new
    end
  end
end
