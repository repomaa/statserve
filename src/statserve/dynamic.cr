module Statserve
  module Dynamic
    def file_handler
      HTTP::StaticFileHandler.new(ENV["STATSERVE_PUBLIC_PATH"], directory_listing: false)
    end
  end
end
