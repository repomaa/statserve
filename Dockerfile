FROM jreinert/crystal-alpine
WORKDIR /opt/statserve
ADD shard.yml shard.lock ./
RUN shards install --production
ADD Makefile ./
ADD src/ ./src/
