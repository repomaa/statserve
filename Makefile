install: bake
	install -Dm755 bin/statserve /bin/statserve

bake: public/
	BAKE_DIR=public shards build --production --release --no-debug --static

.PHONY: bake
